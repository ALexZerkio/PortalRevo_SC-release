@ECHO OFF

COLOR 04
MODE con cols=60 lines=10

ECHO           PortalRevo SC
ECHO            一键安装脚本
ECHO ----------
ECHO     1 安装
ECHO     2 退出
SET PTI=
SET /P PTI=">>"
GOTO %PTI%
exit

:1
echo 安装
::if %errorlevel% == 0 ( ECHO Good ) else ( "C:\Program Files (x86)\Steam\Steam.exe" -applaunch 601360)

::启动 revolution
START steam://rungameid/601360
CLS
ECHO 启动进程

:: 预留 10s 超时
TIMEOUT /T 10 /NOBREAK

:: 通过进程名捕获文件位置并存为 target.temp
wmic process where name="revolution.exe" get executablepath | findstr "revolution.exe" > target.temp
CLS
ECHO 结束进程

:: 预留 3s 超时
TIMEOUT /T 4 /NOBREAK

:: 杀死进程
taskkill -F -IM revolution.exe

:: 赋值给变量
set /P REVO_DIR=<target.temp

:: 解压汉化包发行卷
TAR -XF ".\PortalRevo_SC_latest.tar"
CLS
ECHO 解压发行卷
TIMEOUT /T 2 /NOBREAK

:: 移动（先复制后删除）并覆盖 Portal Revolution 目录
XCOPY ".\Portal Revolution" "%REVO_DIR%\..\..\..\" /H /E /Y
RD ".\Portal Revolution" /S /Q
CLS
ECHO 复制文件
TIMEOUT /T 2 /NOBREAK
CLS
ECHO 安装完成，本窗口将在 3 秒后关闭
ping -n 4 127.0.0.1 > nul



:2
EXIT


